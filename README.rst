=====
String Utils
=====

Features:

* Random strings: random_string, random_ascii, random_int 

Installation
============
    $ pip install -e git+https://minhnhb@bitbucket.org/minhnhb/string-utils.git#egg=string_utils