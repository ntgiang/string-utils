import string
import random

def random_string(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
        return ''.join(random.choice(chars) for x in range(size))

def random_ascii(size=6):
        return random_string(size, string.ascii_uppercase + string.ascii_letters)

def random_int(size=6):
        return random_string(size, string.digits)
        
def random_key(size=6):
        return random_string(size, string.digits + string.ascii_lowercase + "#$%&\()*+,-./:;<=>?@[\\]^_{|}~")
